import tensorflow as tf
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler

from Self_Net_TSP.actor import Actor
from Self_Net_TSP.config import get_config
from Self_Net_TSP.dataset import DataGenerator
from Self_Net_TSP.tsp_with_ortools import Solver

from utils.sampler import Sampler

import numpy as np

sample = Sampler()

grid = sample.get_grid_electricity(size=5, percentage=0.5)

config, _ = get_config()
points = grid[np.where(grid[:, 2] > 0), 0:2]
points = points.reshape((points.shape[1], points.shape[2]))

sampled_points = points.shape[0]

actor = Actor(config)
variables_to_save = [v for v in tf.global_variables() if 'Adam' not in v.name]
saver = tf.train.Saver(var_list=variables_to_save, keep_checkpoint_every_n_hours=1.0)

session = tf.Session()
seed_ = 123

session.run(tf.global_variables_initializer())

# Restore variables from disk.
if config.restore_model:
    saver.restore(session, "Self_Net_TSP/save/" + config.restore_from + "/actor.ckpt")
    print("Model restored.")

solver = Solver(sampled_points)
training_set = DataGenerator(solver)

original_sequence = points
opt_trip, opt_length = training_set.solve_instance(original_sequence)
pca = PCA()
input_batch = pca.fit_transform(points)

scaler = MinMaxScaler()
input_batch = scaler.fit_transform(input_batch)
input_batch_no_padding = input_batch

sequence_max_length = 20
padding_size = sequence_max_length - sampled_points

padding = np.tile(input_batch[0], (padding_size, 1))
input_batch = np.vstack((input_batch, padding))

padding = np.tile(points[0], (padding_size, 1))
points = np.vstack((points, padding))

input_batch = np.tile(input_batch, (actor.batch_size, 1, 1))
feed = {actor.input_: input_batch}
permutations, circuit_length = session.run([actor.positions, actor.distances], feed_dict=feed)

predictions = []
j = np.argmin(circuit_length)
best_permutation = permutations[j][:-1]
predictions.append(circuit_length[j])

print('\n Best tour length:', circuit_length[j])
print(' * permutation:', best_permutation)

# Optimal tour length
opt_trip, opt_length = training_set.solve_instance(input_batch_no_padding)
print('\n Optimal tour length:', opt_length)

# plot corresponding tour
training_set.visualize_2D_trip(opt_trip)
training_set.visualize_2D_trip(points[best_permutation])










