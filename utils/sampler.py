import os
import pickle

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture as GMM


class Sampler(object):
    def __init__(self, data_file='', seed=123, components=4, init=False,
                 pkl_file=os.path.dirname(os.path.realpath(__file__)) + '/../data/for_distributions/demand_gmm.pkl'):
        if init:
            data = pd.read_csv(data_file, header=None)
            data.columns = ['Gas', 'Electricity']
            data['Gas'] = data['Gas'].str.replace(' ', '')
            data['Electricity'] = data['Electricity'].str.replace(' ', '')
            data = data.infer_objects()
            data['Gas'] = pd.to_numeric(data['Gas']) / 3600  # Mjoul to Mwatt
            data['Electricity'] = pd.to_numeric(data['Electricity']) / 3600  # MJoul to MWatt
            self.data = data.copy()
            # self.scaler = MinMaxScaler().fit(X=data.values)
            # self.data_scaled = self.scaler.transform(data.values)
            # self.data_scaled = pd.DataFrame(self.data_scaled, columns=self.data.columns)
            self.mixture = GMM(n_components=components, random_state=None, n_init=100, reg_covar=4e-5).fit(data)

            outfile = open(pkl_file, 'wb')
            pickle.dump(self.mixture, outfile)
            outfile.close()
        np.random.seed(seed)  # set the global seed two
        with open(pkl_file, 'rb') as infile:
            self.mixture = pickle.load(infile)
            self.mixture.random_state = None

    def sample_unscaled(self, n_points=1):
        samples = self.mixture.sample(n_points)
        # return samples[0].clip(min=0) #second element in the tuple are the mixture IDs from which each sample is drawn
        # Get the El demand
        return (samples[0].clip(min=0))[:,1]  # second element in the tuple are the mixture IDs from which each sample is drawn

    def get_grid_electricity(self, size=5, percentage=0.3, n_consumers_=0, normalize_demands=False):
        """
        :param normalize_demands: Normalizes demands to a distribution  d/sum(D) for d in D
        :param size: of a quadratic grid
        :param percentage: what percentage of the grid should be consumers
        :param n_consumers_: Number of desired consumers. THis overrides the percentage setting
        :return: a list in the form [y, x, demand] with the origin of the coordinate system in the top left
        """
        assert percentage < 0.7
        assert percentage > 0.1
        pos_list = np.arange(size ** 2)
        if n_consumers_ < 1:
            n_consumers = np.int(percentage * size * size)
        else:
            n_consumers = n_consumers_
        positions = np.random.choice(pos_list, n_consumers, replace=False)
        # print(len(positions))
        users = np.zeros(size ** 2)
        users[positions] = 1
        users = np.reshape(users, [size, size])
        demands = self.sample_unscaled(size * size)
        demands = np.reshape(demands, [size, size])

        users = demands * users
        original_instance = []
        for i in range(size):
            for j in range(size):
                if users[i, j] > 0.:
                    original_instance.append([i, j, users[i, j]])

        original_instance = np.vstack(original_instance)
        decorrelated_instance, ind = Sampler.decorrelate_instance(original_instance, grid_width=size)
        original_instance = original_instance[ind, :]

        if normalize_demands:
            demands = original_instance[:, 2]
            demands = demands / np.sum(demands.flatten())
            original_instance[:, 2] = demands
            decorrelated_instance[:, 2] = demands

        return original_instance, decorrelated_instance

    @staticmethod
    def decorrelate_instance(original_instance, grid_width=1):

        demands = np.reshape(original_instance[:, 2], [-1, 1])
        pca = PCA()
        decorrelated_instance = pca.fit_transform(original_instance[:, 0:2]) / grid_width
        # demands = demands / np.max(demands)
        # demands = MinMaxScaler().fit_transform(demands)
        decorrelated_instance = np.hstack([decorrelated_instance, demands])
        # DO sort
        ind = np.lexsort((decorrelated_instance[:, 1], decorrelated_instance[:, 0]))
        decorrelated_instance = decorrelated_instance[ind, :]
        return decorrelated_instance, ind

    def sample_number_consumers(self, size=5, percentage=0.3):
        """
        :param size: of a quadratic grid
        :param percentage: what percentage of the grid should be consumers
        :return: a list in the form [y, x, demand] with the origin of the coordinate system in the top left
        """
        assert percentage < 0.7
        assert percentage > 0.1
        pos_list = np.arange(size ** 2)
        n_consumers = np.int(percentage * size * size)
        positions = np.random.choice(pos_list, n_consumers, replace=False)
        users = np.zeros(size ** 2)
        users[positions] = 1
        users = np.reshape(users, [size, size])
        demands = self.sample_unscaled(size * size)
        demands = np.reshape(demands, [size, size])
        users = demands * users
        final_list = []
        for i in range(size):
            for j in range(size):
                if users[i, j] > 0.:
                    final_list.append([i, j, users[i, j]])
        return len(final_list)
