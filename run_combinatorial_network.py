# !/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import os
from datetime import datetime

import matplotlib
import tensorflow as tf
import tqdm
import numpy as np
from matplotlib.pyplot import savefig
import matplotlib.pyplot as plt

from combinatorial_network.config import get_config
from combinatorial_network.actor import Actor
from combinatorial_network.game import GameCombinatorialNetwork
from utils.misc import visualize_2d_trip_n, create_batch_from_csv, normalize_costs

N = 100000.0
config, _ = get_config()

max_episodes = config.nb_epoch

original_batch_input = None
decorrelated_batch_input = None

include_separator = True
include_separator_dimension = int(include_separator)
input_dimension = 2  # + include_separator_dimension

omit_demands = False
input_dimension += int(not omit_demands)
batch_size = config.batch_size

game = GameCombinatorialNetwork()

grid_width = 0
use_csv_input = True
if len(config.input_file) > 0:
    consumers, original_batch_input, decorrelated_batch_input = \
        create_batch_from_csv(game,
                              batch_size,
                              config.input_file, include_separator=include_separator)
    use_csv_input = True
else:
    grid_width = config.grid_width
    consumers = config.max_length

sequence_length = consumers + int(include_separator)  # separator

hidden_dim = config.hidden_dim
num_heads = config.num_heads
num_stacks = config.num_stacks
inference_mode = config.inference_mode
temperature = config.temperature
logit_clip = config.C
init_baseline = float(consumers) / 2.0

alpha = config.alpha

lr1_start = config.lr1_start
lr1_decay_step = config.lr1_decay_step
lr1_decay_rate = config.lr1_decay_rate

restore_model = config.restore_model
restore_from = config.restore_from

save_every_n_episodes = config.save_every_n_episodes

actor = Actor(batch_size,
              sequence_length,
              input_dimension,
              hidden_dim,
              num_heads,
              num_stacks,
              inference_mode,
              temperature,
              logit_clip,
              init_baseline,
              alpha,
              lr1_start,
              lr1_decay_rate,
              lr1_decay_step)

with tf.Session() as sess:
    now = datetime.now()
    #if len(config.log_dir_timestamp) <= 0:
    #    time = now.strftime("%Y%m%d-%H%M%S")
    #else:
    #    time = config.log_dir_timestamp
    time = now.strftime("%Y%m%d-%H%M%S")
    train_writer = tf.summary.FileWriter('%s/%s/' % (config.log_dir, time), sess.graph)

    sess.run(tf.global_variables_initializer())

    # Saver to save & restore all the variables except for optimizer
    variables_to_save = [v for v in tf.global_variables() if 'Adam' not in v.name]

    saver = tf.train.Saver(var_list=variables_to_save, keep_checkpoint_every_n_hours=1.0)
    if restore_model or inference_mode:
        saver.restore(sess, config.restore_from)

    if not inference_mode:
        matplotlib.use('Agg')
        global_step = sess.run(actor.global_step)

        validation_batches_count = config.validation_batches
        validation_batches = [game.generate_instance_repeated(batch_size,
                                                              grid_width,
                                                              consumers,
                                                              include_separator,
                                                              omit_demands) for _ in range(validation_batches_count)]

        for episode in tqdm.tqdm(iterable=range(global_step, max_episodes), initial=global_step, total=max_episodes):
            original_batch_input, decorrelated_batch_input = \
                game.generate_instance_repeated(batch_size,
                                                grid_width,
                                                consumers,
                                                include_separator,
                                                omit_demands)

            points_wo_separator = original_batch_input[0, :-1, :]
            cluster1, cluster2 = game.compute_clustering_partitions(points_wo_separator)

            _, _, _, c1, c2 = game.compute_tsp_and_costs(cluster1, cluster2)

            costs_heuristic = [normalize_costs(c1), normalize_costs(c2)]

            feed_dict = {actor.input_: decorrelated_batch_input,
                         actor.original_input_: original_batch_input,
                         actor.is_training_: not config.inference_mode,
                         actor.costs_heuristic_: costs_heuristic}

            positions, costs, summary, base_op, train_step, train_step2 = sess.run([actor.positions,
                                                                                    actor.costs,
                                                                                    actor.merged,
                                                                                    actor.base_op,
                                                                                    actor.train_step1,
                                                                                    actor.train_step2],
                                                                                   feed_dict=feed_dict)
            if episode > 0 and episode % 15 == 0:
                train_writer.add_summary(summary, episode)

            if episode > 0 and episode % config.save_every_n_episodes == 0:
                os.makedirs(config.save_to, exist_ok=True)
                saver.save(sess, config.save_to + '/actor.ckpt', global_step=episode)

                all_heuristic_costs = [-1]
                all_current_costs = [episode]
                for i, (o, d) in enumerate(validation_batches):
                    feed_dict = {actor.input_: d, actor.original_input_: o, actor.is_training_: False}

                    positions, costs = sess.run([actor.positions,
                                                 actor.costs], feed_dict=feed_dict)

                    j = np.argmin(costs)

                    curr_costs = costs[j]
                    circuit = positions[j, :-1]

                    trips = []

                    points = o[j, :, :]

                    # Our partition raw
                    circuit1, circuit2, _, _ = game.extract_partitions(points,
                                                                       circuit,
                                                                       sequence_length - 1,
                                                                       sequence_length)

                    trip_length_1 = GameCombinatorialNetwork.compute_travel_costs(circuit1)
                    trip_length_2 = GameCombinatorialNetwork.compute_travel_costs(circuit2)

                    if len(circuit1) > 0:
                        demands_1 = np.sum(circuit1[:, 2])
                    else:
                        demands_1 = 0.0

                    if len(circuit2) > 0:
                        demands_2 = np.sum(circuit2[:, 2])
                    else:
                        demands_2 = 0.0

                    _, c1, c2 = GameCombinatorialNetwork.cost_function(demands_1, demands_2, trip_length_1,
                                                                       trip_length_2)
                    costs_our = normalize_costs(c1) + normalize_costs(c2)

                    # Heuristic
                    points_wo_separator = points[:-1, :]

                    cluster1, cluster2 = game.compute_clustering_partitions(points_wo_separator)
                    _, cluster1_trip, cluster2_trip, c1, c2 = game.compute_tsp_and_costs(cluster1, cluster2)

                    cluster_circuit1 = cluster1[cluster1_trip, :]
                    cluster_circuit2 = cluster2[cluster2_trip, :]

                    costs_heuristic = normalize_costs(c1) + normalize_costs(c2)
                    all_heuristic_costs.append(costs_heuristic)

                    ours_better = int(costs_our <= costs_heuristic)
                    trips.append(([circuit1, circuit2], "%s: %.3f (%d)" % ('Ours', costs_our, ours_better)))
                    trips.append(([cluster_circuit1, cluster_circuit2], "%s: %.3f" % ('Heuristic', costs_heuristic)))

                    checkpoints_plots_dir = config.save_to + '/plots/%d/' % episode
                    os.makedirs(checkpoints_plots_dir, exist_ok=True)

                    visualize_2d_trip_n(*trips, show=False)
                    savefig("%s/%i.png" % (checkpoints_plots_dir, i))
                    plt.close('all')

                    all_current_costs.append(curr_costs)
                results_file = config.save_to + '/plots/results.csv'
                file_existed = os.path.isfile(results_file)

                with open(results_file, 'a+') as f:
                    writer = csv.writer(f)
                    if not file_existed:
                        writer.writerow(all_heuristic_costs)
                    writer.writerow(all_current_costs)
    else:
        plots_dir = config.save_plots_to
        os.makedirs('%s' % plots_dir, exist_ok=True)
        if not use_csv_input:
            examples = config.examples
        else:
            examples = 1
        better = 0

        solutions = []
        savings = 0
        for i in range(examples):
            if not use_csv_input:
                original_batch_input, decorrelated_batch_input = game.generate_instance_repeated(batch_size,
                                                                                                 grid_width,
                                                                                                 consumers,
                                                                                                 include_separator,
                                                                                                 omit_demands)

            # Heuristic
            points = original_batch_input[0, :, :]
            points_wo_separator = points[:-1, :]
            cluster1, cluster2 = game.compute_clustering_partitions(points_wo_separator)
            costs_heuristic, cluster1_trip, cluster2_trip, c1, c2 = game.compute_tsp_and_costs(cluster1, cluster2)
            cluster_circuit1 = cluster1[cluster1_trip, :]
            cluster_circuit2 = cluster2[cluster2_trip, :]
            trips2 = [cluster_circuit1, cluster_circuit2]
            costs_heuristic /= N
            costs_heuristic_both = [normalize_costs(c1), normalize_costs(c2)]
            # ---

            feed = {actor.input_: decorrelated_batch_input, actor.original_input_: original_batch_input,
                    actor.is_training_: False}
            j = 0
            costs = [999999999999999999]
            circuit = None
            permutations = None
            for test_times in tqdm.tqdm(range(4)):
                tf.random.set_random_seed(np.random.randint(1,20000))
                permutations_, costs_ = sess.run([actor.positions, actor.costs], feed_dict=feed)
                j_ = np.argmin(costs)
                circuit_ = permutations_[j_, :-1]
                if costs_[j_] < costs[j]:
                    j = j_
                    costs = costs_
                    circuit = circuit_
                    permutations = permutations_

            points = original_batch_input[j, :, :]
            # Our partition raw
            circuit1, circuit2, _, _ = game.extract_partitions(points,
                                                               circuit,
                                                               sequence_length - 1,
                                                               sequence_length)

            trips1 = [circuit1, circuit2]
            trip_length_1 = GameCombinatorialNetwork.compute_travel_costs(circuit1)
            trip_length_2 = GameCombinatorialNetwork.compute_travel_costs(circuit2)
            demands_1 = np.sum(circuit1[:, 2])
            demands_2 = np.sum(circuit2[:, 2])
            costs_our, c1, c2 = GameCombinatorialNetwork.cost_function(demands_1, demands_2, trip_length_1,
                                                                       trip_length_2)
            costs_our /= N
            # ---

            # Our partition with proper TSP
            costs_2, tsp_ours_1, tsp_ours_2, _, _ = game.compute_tsp_and_costs(circuit1, circuit2)
            circuit1_2 = circuit1[tsp_ours_1, :]
            circuit2_2 = circuit2[tsp_ours_2, :]
            trips3 = [circuit1_2, circuit2_2]
            costs_2 /= N

            ours_better = int(costs_our <= costs_heuristic)

            visualize_2d_trip_n((trips1, "Ours (%.2f)" % costs_our),
                                (trips2, "Clustering (%.2f)" % costs_heuristic),
                                (trips3, 'Ours + TSP (%.2f)' % costs_2), show=config.show_plots)

            if config.save_plots and len(config.save_plots_to) > 0:
                savefig("%s/%s.png" % (plots_dir, config.save_name))
            plt.close('all')

            s1 = np.hstack([circuit1_2, np.zeros([len(circuit1_2), 1])])
            s2 = np.hstack([circuit2_2, np.ones([len(circuit2_2), 1])])

            s1_heuristic = np.hstack([cluster_circuit1, np.zeros([len(cluster_circuit1), 1])])
            s2_heuristic = np.hstack([cluster_circuit2, np.ones([len(cluster_circuit2), 1])])
            solution_heuristic = np.vstack([s1_heuristic, s2_heuristic])

            solution = np.vstack([s1, s2])

            filename = "%s/%s.csv" % (config.save_solutions_to, config.save_name)
            filename_heuristic = "%s_heuristic/%s.csv" % (config.save_solutions_to, config.save_name)

            if len(config.save_solutions_to) > 0 and config.save_solutions:
                # noinspection PyTypeChecker
                np.savetxt(filename, solution, delimiter=',')

            if len(config.save_solutions_to) > 0 and config.save_solutions:
                # noinspection PyTypeChecker
                np.savetxt(filename_heuristic, solution_heuristic, delimiter=',')

            better += ours_better
            savings += costs_heuristic - costs_our

            print("-------------------------")
            print("Ours: %.2f (%d)" % (costs_our, ours_better))
            print("Ours + TSP: %.2f" % costs_2)
            print("Theirs: %.2f" % costs_heuristic)
            print("-------------------------")

        performance = better / examples

        print('Performance: %.2f' % performance)
        print('Saved resources: %.2f' % savings)
