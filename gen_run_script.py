execution_string = ''

for i in range(0, 19):
    input_f = 'evaluation/input_data/{}.csv'.format(i)
    exec_line = 'python ./run_combinatorial_network.py --input_file="./{}" --save_solutions_to="./evaluation/first_level_results" --save_name="{}"\n'.format(input_f, i, i)
    execution_string += exec_line

print(execution_string)
text_file = open("./run_level_1.sh", "w")
text_file.write(execution_string)
text_file.close()
