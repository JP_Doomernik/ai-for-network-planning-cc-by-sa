import argparse
import os

# mpl.rc('image', cmap='jet')  # Set nicer colormap
from evaluation.eval_lib import process_file
import pandas as pd
import numpy as np

firstLevel = False
# Inputs
fl_input_dir = 'first_level_results/'
fl_input_dir_benchmark = 'first_level_results_heuristic/'
sl_input_dir = 'second_level_results/'
sl_input_dir_benchmark = 'second_level_results_benchmark_heuristic/'

# Outputs
tdir = 'processed/'
fl_out = tdir + 'fl_final/'
fl_out_benchmark = tdir + 'fl_final_benchmark/'
sl_out = tdir + 'sl_final/'
sl_out_benchmark = tdir + 'sl_final_benchmark/'
fl_plt = tdir + 'fl_plots/'
fl_plt_benchmark = tdir + 'fl_plots_benchmark/'

sl_plt = tdir + 'sl_plots/'
sl_plt_benchmark = tdir + 'sl_plots_benchmark/'

# Final Concatenated
all_res = tdir + 'final_res/'

# Dir for first level plots

# Second level
# Dir for the second level resutls

if __name__ == '__main__':
    # Do the argparse

    file_list1 = [os.path.join(fl_input_dir, f) for f in os.listdir(fl_input_dir) if
                  os.path.isfile(os.path.join(fl_input_dir, f))]
    file_list1_benchmark = [os.path.join(fl_input_dir_benchmark, f) for f in os.listdir(fl_input_dir_benchmark) if
                            os.path.isfile(os.path.join(fl_input_dir_benchmark, f))]

    file_list2 = [os.path.join(sl_input_dir, f) for f in os.listdir(sl_input_dir) if
                  os.path.isfile(os.path.join(sl_input_dir, f))]
    file_list2_benchmark = [os.path.join(sl_input_dir_benchmark, f) for f in os.listdir(sl_input_dir_benchmark) if
                            os.path.isfile(os.path.join(sl_input_dir_benchmark, f))]

    if firstLevel:
        connections = []
        producers = []
        for i, f, in enumerate(file_list1):
            if "heur" in f:
                print(i)
            connections_, producers_ = process_file(f, i, fl_plt)
            connections.append(connections_)
            producers.append(producers_)
        connections = np.vstack(connections)
        producers = np.vstack(producers)
        res_dir = fl_out
        connections_benchmark = []
        producers_benchmark = []

        for i, f, in enumerate(file_list1_benchmark):
            if "heur" in f:
                print(i)
            connections_, producers_ = process_file(f, i, fl_plt_benchmark)
            connections_benchmark.append(connections_)
            producers_benchmark.append(producers_)
        connections_benchmark = np.vstack(connections_benchmark)
        producers_benchmark = np.vstack(producers_benchmark)
        res_dir_benchmark = fl_out_benchmark
        res_dur = fl_out

    else:
        if firstLevel:
            level = 1
        else:
            level = 2
        connections, producers = process_file(file_list2[0], 0, sl_plt, level=level)
        connections_benchmark, producers_benchmark = process_file(file_list2_benchmark[0], 0, sl_plt_benchmark, level=2)
        res_dir_benchmark = sl_out_benchmark
        res_dir = sl_out

    connections = pd.DataFrame(connections, columns=['x1', 'y1', 'x2', 'y2', 'distance', 'capacity'])
    producers = pd.DataFrame(producers, columns=['x', 'y', 'capacity'])

    connections.to_csv(res_dir + "connections.csv", index=None)
    producers.to_csv(res_dir + "producers.csv", index=None)
    producers.to_csv(res_dir + "producers_inp.csv", index=None, header=None)

    connections_benchmark = pd.DataFrame(connections_benchmark,
                                         columns=['x1', 'y1', 'x2', 'y2', 'distance', 'capacity'])
    producers_benchmark = pd.DataFrame(producers_benchmark, columns=['x', 'y', 'capacity'])
    connections_benchmark.to_csv(res_dir_benchmark + "connections_benchmark.csv", index=None)
    producers_benchmark.to_csv(res_dir_benchmark + "producers_benchmark.csv", index=None)
    producers_benchmark.to_csv(res_dir_benchmark + "producers_inp_benchmark.csv", index=None, header=None)

    # Do merge first and second level
    if not firstLevel:
        conn_first_l = pd.read_csv(fl_out + "connections.csv")
        prod_first_l = pd.read_csv(fl_out + "producers.csv")

        conn_first_benchmark = pd.read_csv(fl_out_benchmark + "connections_benchmark.csv")
        prod_first_benchmark = pd.read_csv(fl_out_benchmark + "producers_benchmark.csv")

        connections_all = pd.concat([conn_first_l, connections])
        connections_all_benchmark = pd.concat([conn_first_benchmark, connections_benchmark])

        prod_all = pd.concat([producers])
        prod_all_benchmark = producers_benchmark

        connections_all.to_csv('processed/all_res/connections.csv', index=None)
        prod_all.to_csv('processed/all_res/producers.csv', index=None)

        connections_all_benchmark.to_csv('processed/all_res/connections_benchmark.csv', index=None)
        prod_all_benchmark.to_csv('processed/all_res/producers_benchmark.csv', index=None)
