import pandas as pd
import os

gridpoints = pd.read_csv('./groningen_grid8.csv')
gridsize = 37.5
latstep = gridsize/111267.31
lonstep = gridsize/68677.99

def coordinateFinderTodor(x, y):
    lon = 6.55586125e+00 +  3.79009502e-04*x -3.80605350e-07*y
    lat = 5.32124297e+01 +  1.08240702e-07*x  + 3.17917062e-04*y
    return lat, lon

def writeemptygridkmlandwritedefinitionfile(filename, connections):
    #open the kml-file (note it's a simple text-file with .kml at the end)
    f = open(filename, 'w')
    #write some definitions to make google earth understand our messy lines
    f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    f.write('<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"><Document>\n')
    #loop over de connections to be drawn
    for i in range(len(connections)):
        f.write('<Placemark>\n')
        lat1 = connections[i][0]
        lon1 = connections[i][1]
        lat2 = connections[i][2]
        lon2 = connections[i][3]
        thickness = connections[i][4]
        f.write('<Style><LineStyle><width>'+str(thickness)+'</width></LineStyle></Style>\n')
        f.write('<LineString><tessellate>1</tessellate><coordinates>\n')
        f.write(str(lon1)+','+str(lat1)+',0\n')
        f.write(str(lon2)+','+str(lat2)+',0\n')
        #close the xml body of the placemark
        f.write('</coordinates></LineString></Placemark>\n')
    #write the closing statements of the KML-document body and close the file
    f.write('</Document></kml>')
    f.close()

def writegridkml(filename, squares):
    f = open(filename, 'w')
    f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    f.write('<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom"><Document>\n')
    f.write('<Style id="rood"><LineStyle><width>0</width></LineStyle><PolyStyle><color>ff0000ff</color></PolyStyle></Style>\n')
    f.write('<Style id="oranje"><LineStyle><width>0</width></LineStyle><PolyStyle><color>ff0080ff</color></PolyStyle></Style>\n')
    f.write('<Style id="geel"><LineStyle><width>0</width></LineStyle><PolyStyle><color>ff00ffff</color></PolyStyle></Style>\n')
    f.write('<Style id="geelgroen"><LineStyle><width>0</width></LineStyle><PolyStyle><color>ff00ff80</color></PolyStyle></Style>\n')
    f.write('<Style id="age"><LineStyle><width>0</width></LineStyle><PolyStyle><color>ff00aaff</color></PolyStyle></Style>\n')#ffaa00
    f.write('<Style id="blauw"><LineStyle><width>0</width></LineStyle><PolyStyle><color>ffff0000</color></PolyStyle></Style>\n')
    #print np.shape(values), lonsteps, latsteps

    
    #f.write('<name>'+str(j)+'_'+str(i)+'</name>\n')
    #f.write('<description>'+namen[l]+'\n'+waarden[l]+'kWh</description>\n')
     
    
    deltalon = lonstep/2.
    deltalat = latstep/2.
    for polys in squares:
        f.write('<Placemark>\n')
        f.write('<styleUrl>#oranje</styleUrl>\n')
        f.write('<Polygon><extrude>1</extrude><tessellate>1</tessellate><altitudeMode>relativeToGround</altitudeMode><outerBoundaryIs><LinearRing><coordinates>\n')
        f.write(str(polys[0]-deltalon)+','+str(polys[1]-deltalat)+','+str(polys[2])+'\n')
        f.write(str(polys[0]-deltalon)+','+str(polys[1]+deltalat)+','+str(polys[2])+'\n')
        f.write(str(polys[0]+deltalon)+','+str(polys[1]+deltalat)+','+str(polys[2])+'\n')
        f.write(str(polys[0]+deltalon)+','+str(polys[1]-deltalat)+','+str(polys[2])+'\n')
        f.write(str(polys[0]-deltalon)+','+str(polys[1]-deltalat)+','+str(polys[2])+'\n')
        f.write('</coordinates></LinearRing></outerBoundaryIs></Polygon></Placemark>\n')
    f.write('</Document></kml>')
    f.close()


folder = './'

x = os.listdir(folder)

for filen in x:
    if filen.endswith('producers.csv'):
            
        connectionfile = pd.read_csv(folder+filen)
        connections = []
#        print connectionfile.index

        for index in connectionfile.index:
            lat1, lon1 = coordinateFinderTodor(connectionfile.x[index], connectionfile.y[index])
            thickness = connectionfile.capacity[index]
            connections.append([lon1, lat1, 10])

        writegridkml('kmlfiles/producers2.kml', connections)
 #       print 'done'


