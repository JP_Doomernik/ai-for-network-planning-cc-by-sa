import tqdm

from combinatorial_network.game import GameCombinatorialNetwork
import numpy as np

from utils.misc import compute_optimal_trip, bin_array, visualize_2d_trip_2

game = GameCombinatorialNetwork()

consumers = 6
batch_size = 10
grid_width = 12
instances_original, instances = game.generate_instance(batch_size, grid_width, consumers, False, False)

permutations_count = 2 ** consumers
costs = np.zeros([batch_size, permutations_count, 3])

heuristic_costs = []
solutions = []
for i in range(len(instances)):
    points = instances[i, :]

    h_p1, h_p2 = GameCombinatorialNetwork.compute_clustering_partitions(points)
    current_heuristic_costs = np.log(GameCombinatorialNetwork.compute_partition_costs(h_p1, h_p2))
    _, h_trip1, _ = compute_optimal_trip(h_p1)
    _, h_trip2, _ = compute_optimal_trip(h_p2)

    h_p1 = h_p1[h_trip1, :]
    h_p2 = h_p2[h_trip2, :]

    heuristic_costs.append(current_heuristic_costs)
    visited = {}
    for k in tqdm.tqdm(range(permutations_count)):
        permutation = bin_array(k, consumers)
        neg_permutation = 1 - permutation

        if k not in visited:
            visited[k] = 1

            complement = sum(b*2**i for i, b in enumerate(neg_permutation))
            visited[complement] = 1

            p1 = points[np.where(permutation)]
            p2 = points[np.where(1-permutation)]

            if len(p1) <= 0 or len(p2) <= 0:
                continue
            current_costs = np.log(GameCombinatorialNetwork.compute_partition_costs(p1, p2))
            # print("Heuristic Costs: %.2f, Current Costs: %.2f" % (current_heuristic_costs, current_costs))

            costs[i, k, 0] = current_costs
            costs[i, k, 1] = min([len(p1), len(p2)])
            costs[i, k, 2] = int(current_costs < current_heuristic_costs)

            if current_costs < current_heuristic_costs:

                _, trip1, _ = compute_optimal_trip(p1)
                _, trip2, _ = compute_optimal_trip(p2)

                p1 = p1[trip1, :]
                p2 = p2[trip2, :]

                if len(p1) == len(p2):
                    solution = np.hstack(
                        [np.vstack([p1[:-1], p2[:-1]]),
                         np.expand_dims(permutation, axis=1)])

                    solutions.append(solution)

                # visualize_2d_trip_2([p1, p2], [h_p1, h_p2])