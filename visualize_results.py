import matplotlib.pyplot as plt
import csv
import numpy as np
import urllib.request

url = 'http://35.193.112.178/plots/plots/results.csv'
urllib.request.urlretrieve(url, 'results.csv')

input_file = 'save/models/25_critic/plots/results.csv'
# input_file = 'results.csv'
with open(input_file) as f:
    reader = csv.reader(f, delimiter=',')
    data = [list(map(float, r)) for r in reader]

plt.figure(figsize=(10, 10))
plt.style.use('seaborn-darkgrid')

data = np.array(data)
episodes = data[1:, 0]

heuristic_costs = data[0, 1:]

costs = data[1:, 1:]

average_costs = np.sum(data[1:, 1:], axis=1)
average_heuristic_costs = np.sum(data[0, 1:])

num_validation_samples = costs.shape[1]
grid_width = np.ceil(np.sqrt(num_validation_samples + 1))
grid_height = grid_width

while grid_width * grid_height >= num_validation_samples:
    grid_height -= 1

grid_height += 1

for i in range(num_validation_samples):
    plt.subplot(grid_width, grid_height, i+1)
    plt.plot(episodes, costs[:, i])
    plt.plot([episodes[0], episodes[-1]], [heuristic_costs[i], heuristic_costs[i]], '--r')
    plt.title('Instance %d' % i)

plt.subplot(grid_width, grid_height, num_validation_samples+1)
plt.plot(episodes, average_costs, '-g')
plt.plot([episodes[0], episodes[-1]], [average_heuristic_costs, average_heuristic_costs], '--r')
plt.title('Average')

plt.suptitle('Costs over Epochs')
plt.subplots_adjust(hspace=0.5)
plt.show()
