import tensorflow as tf

from combinatorial_network.encoder import AttentiveEncoder


class Critic(object):
    def __init__(self, encoder):
        """
        :type encoder: AttentiveEncoder
        """

        # Network config
        self.num_neurons = encoder.input_embed  # dimension of hidden states (LSTM cell)
        self.initializer = tf.contrib.layers.xavier_initializer()  # variables initializer

        # Baseline setup
        self.init_baseline = 0.  # self.max_length/2 # good initial baseline for TSP
        self.encoder = encoder
        self.predictions = None

    def predict_rewards(self, input_):
        with tf.variable_scope("encoder"):
            encoder_output = self.encoder.encode(input_)
            frame = tf.reduce_mean(encoder_output,
                                   1)  # [Batch size, Sequence Length, Num_neurons] to [Batch size, Num_neurons]

        with tf.variable_scope("ffn"):
            # ffn 1
            h0 = tf.layers.dense(frame, self.num_neurons, activation=tf.nn.relu, kernel_initializer=self.initializer)
            # ffn 2
            w1 = tf.get_variable("w1", [self.num_neurons, 1], initializer=self.initializer)
            b1 = tf.Variable(self.init_baseline, name="b1")
            self.predictions = tf.squeeze(tf.matmul(h0, w1) + b1)
            stop = 1
