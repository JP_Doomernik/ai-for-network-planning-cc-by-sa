import gc
import multiprocessing

import numpy as np
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import pairwise_distances

from Self_Net_TSP.tsp_with_ortools import Solver
from utils.sampler import Sampler


class GameCombinatorialNetwork:
    def __init__(self):
        self.SEPARATOR = np.expand_dims(np.array([-1, -1, -1]), axis=0)

        self.cpu_count = multiprocessing.cpu_count()
        self.sampler = Sampler(seed=1337)

    def generate_instance(self, batch_size, grid_width, consumers, include_separator=True, omit_demands=False,
                          normalize_demands=True):
        original_batch = []
        decorrelated_batch = []

        # Some samples have demand 0 and crash the batch generation. Discard them
        while len(original_batch) < batch_size:
            b, d = self.sampler.get_grid_electricity(grid_width, 0.4, consumers, normalize_demands=False)

            if normalize_demands:
                demands = d[:, 2]
                demands = demands / np.sum(demands.flatten())
                d[:, 2] = demands

            # This is used for training in the combinatorial network
            #
            if include_separator:
                b = np.concatenate([b, self.SEPARATOR], axis=0)
                d = np.concatenate([d, self.SEPARATOR], axis=0)

            if omit_demands:
                b = b[:, :2]
                d = d[:, :2]

            # print(len(b))
            if len(b) == consumers + int(include_separator):
                original_batch.append(b)
                decorrelated_batch.append(d)

        original_batch = np.array(original_batch)
        decorrelated_batch = np.array(decorrelated_batch)

        return original_batch, decorrelated_batch

    def generate_instance_repeated(self, batch_size, grid_width, consumers, include_separator=True, omit_demands=False,
                                   normalize_demands=True):
        instances = self.generate_instance(1, grid_width, consumers, include_separator, omit_demands, normalize_demands)

        result = []
        for instance in instances:
            result.append(np.tile(instance, [batch_size, 1, 1]))

        return result

    @staticmethod
    def play_batch_game_with_separator(predicted_partitions, batch_input):
        sequence_length = predicted_partitions.shape[1]
        separator_index = sequence_length - 1

        rewards = []
        input_dimension = batch_input.shape[2]
        for row in range(predicted_partitions.shape[0]):

            positions = predicted_partitions[row]
            points = batch_input[row, :, :]
            predicted_partition_1, predicted_partition_2, _, _ = \
                GameCombinatorialNetwork.extract_partitions(points,
                                                            positions,
                                                            separator_index,
                                                            sequence_length)

            partition_costs = \
                GameCombinatorialNetwork.compute_partition_costs(predicted_partition_1, predicted_partition_2,
                                                                 GameCombinatorialNetwork.cost_function_simple,
                                                                 input_dimension)

            # point without separator
            # points = np.concatenate([batch_input[row, p1_indices, :], batch_input[row, p2_indices, :]])
            # clustering_partition_1, clustering_partition_2 = \
            #    GameCombinatorialNetwork.compute_random_partitions(points)

            # compute partitions by clustering
            # clustering_partition_1, clustering_partition_2 = \
            #    GameCombinatorialNetwork.compute_clustering_partitions(points)

            # opponent_partition_costs = \
            #    GameCombinatorialNetwork.compute_partition_costs(clustering_partition_1, clustering_partition_2)

            reward = partition_costs
            rewards.append(reward)

        return np.array(rewards)

    @staticmethod
    def play_batch_game(predicted_partitions, batch_input):
        opponent_partition_costs = 0
        costs = []
        for row in range(predicted_partitions.shape[0]):
            partitioning = predicted_partitions[row, :, 0]
            points = batch_input[row, :]

            partition1 = points[np.where(partitioning)]
            partition2 = points[np.where(1 - partitioning)]

            our_partition_costs = \
                GameCombinatorialNetwork.compute_partition_costs(partition1, partition2)

            # point without separator
            # points = np.concatenate([batch_input[row, p1_indices, :], batch_input[row, p2_indices, :]])
            # clustering_partition_1, clustering_partition_2 = \
            #    GameCombinatorialNetwork.compute_random_partitions(points)
            # compute partitions by clustering
            if row == 0:
                clustering_partition_1, clustering_partition_2 = \
                    GameCombinatorialNetwork.compute_clustering_partitions(points)

                opponent_partition_costs = \
                    GameCombinatorialNetwork.compute_partition_costs(clustering_partition_1, clustering_partition_2)

            costs.append(np.array([our_partition_costs, opponent_partition_costs]))

        return np.array(costs).astype(np.float32)

    @staticmethod
    def extract_partitions(points, positions, separator_index, sequence_length):
        b = int(np.where(positions == separator_index)[0])
        p1_indices = []
        p2_indices = []
        if 0 < b:
            p1_indices = positions[0:b]
        if b < sequence_length - 1:
            p2_indices = positions[(b + 1):]
        predicted_partition_1 = points[p1_indices, :] if len(p1_indices) > 0 else []
        predicted_partition_2 = points[p2_indices, :] if len(p2_indices) > 0 else []
        return predicted_partition_1, predicted_partition_2, p1_indices, p2_indices

    @staticmethod
    def play_batch_game_single_tsp(predicted_partitions, batch_input):

        rewards = []
        for row in range(predicted_partitions.shape[0]):
            positions = predicted_partitions[row]
            points = batch_input[row, positions]

            reward = GameCombinatorialNetwork.compute_travel_costs(points)

            rewards.append(reward)

        return np.array(rewards)

    @staticmethod
    def cost_function(demand1, demand2, tsp_len1, tsp_len2):

        # Note here demand needs to be >0 so that it has consumers! otherwise we do not have any producers
        con1 = (151441 * int(demand1 > 0) + 3600 * demand1) * tsp_len1  # Nice since linear
        p1 = 600000 * int(demand1 > 0) + 295000 * demand1
        c1 = con1 + p1

        con2 = (151441 * int(demand2 > 0) + 3600 * demand2) * tsp_len2
        p2 = 600000 * int(demand2 > 0) + 295000 * demand2
        c2 = con2 + p2
        cost = c1 + c2
        return cost, c1, c2

    @staticmethod
    def cost_function_simple(_, __, tsp_len1, tsp_len2):
        return tsp_len1 + tsp_len2

    @staticmethod
    def compute_travel_costs(points):
        distance = 0.0
        if len(points) > 1:
            points = np.vstack([points, np.expand_dims(points[0, :], axis=0)])
            delta_x = np.square(points[:-1, 0] - points[1:, 0])
            delta_y = np.square(points[:-1, 1] - points[1:, 1])

            distance = np.sum(np.sqrt(delta_x + delta_y), axis=0)

        return distance

    @staticmethod
    def compute_clustering_partitions(input_):
        points = input_[:, :2]
        clustering = AgglomerativeClustering(n_clusters=2).fit(points).labels_
        p1 = []
        p2 = []
        for i in range(len(clustering)):
            s = clustering[i]
            if s == 0:
                p1.append(input_[i, :])
            else:
                p2.append(input_[i, :])

        return np.array(p1), np.array(p2)

    @staticmethod
    def compute_random_partitions(input_):
        b = np.random.randint(2, size=len(input_))
        return input_[np.where(b)], input_[np.where(1-b)]

    @staticmethod
    def compute_partition_costs(p1, p2, cost_function=None, input_dimension=3):
        """
        At the very to a batch wrapper
        There are going to be two wrappers. One to estimate the E[Reward|p_o:N]
        Does not take a batch. Takaes only one distribution
        :param input_dimension: input dimension
        :param cost_function: a function that maps the chosen tours and the demands to a cost value
        :param p1: Partition 1
        :param p2: Partition 2
        :return:
        """

        if cost_function is None:
            cost_function = GameCombinatorialNetwork.calculate_costs

        # This fixed the input
        c1 = cost_function(p1, input_dimension)
        c2 = cost_function(p2, input_dimension)

        return c1 + c2

    @staticmethod
    def calculate_costs(p1, input_dimension):
        if len(p1) > 0:
            p1 = np.array(p1)

            tsp_len1, _ = GameCombinatorialNetwork.compute_tsp(p1)
            c1 = GameCombinatorialNetwork.cost_function_one_partition(input_dimension, p1, tsp_len1)

            return c1

        return 0.0

    @staticmethod
    def cost_function_one_partition(input_dimension, points, travel_distance):
        cumulative_demand = 0
        if input_dimension > 2:
            if len(points) > 0:
                cumulative_demand = np.sum(points[:, 2])
        con1 = (151441 * int(cumulative_demand > 0) + 3600 * cumulative_demand) * travel_distance  # Nice since linear
        producer = 600000 * int(cumulative_demand > 0) + 295000 * cumulative_demand
        c1 = con1 + producer
        return c1

    @staticmethod
    def compute_tsp(p1):
        tsp_len1 = 0
        order1 = [0, 0]
        l1 = len(p1)
        #TODO change as in the other branch
        if l1 > 1:
            dm1 = pairwise_distances(p1[:, :2])
            s1 = Solver(tsp_size=l1)
            order1, tsp_len1 = s1.run(dm1)
        return tsp_len1, order1

    @staticmethod
    def compute_tsp_and_costs(p1, p2):
        tsp_len1, order1 = GameCombinatorialNetwork.compute_tsp(p1)
        c1 = GameCombinatorialNetwork.cost_function_one_partition(3, p1, tsp_len1)
        tsp_len2, order2 = GameCombinatorialNetwork.compute_tsp(p2)
        c2 = GameCombinatorialNetwork.cost_function_one_partition(3, p2, tsp_len2)

        costs = c1 + c2

        return costs, order1, order2, c1, c2
