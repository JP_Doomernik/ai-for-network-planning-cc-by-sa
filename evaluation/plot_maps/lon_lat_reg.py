import pandas as pd
from statsmodels.formula.api import ols

data=pd.read_csv('./groningen_grid8.csv')

print(data.head())
model = ols("lon ~ x + y", data).fit()
print(model.summary())

print("\nRetrieving manually the parameter estimates:")
print(model._results.params)

model = ols("lat ~ x + y", data).fit()

print("\nRetrieving manually the parameter estimates:")
print(model._results.params)