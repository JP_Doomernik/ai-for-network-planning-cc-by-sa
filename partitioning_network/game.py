import gc
import multiprocessing

import numpy as np
from joblib import Parallel, delayed
from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import pairwise_distances
from tqdm import tqdm

from Self_Net_TSP.tsp_with_ortools import Solver
from utils.sampler import Sampler


class Game(object):
    def __init__(self, size=12):
        self.sampler = Sampler()
        self.size = size

        # TODO Change those
        self.cost_cable_unit_per_mw = 10
        self.fixed_cost_prod = 1177
        self.variable_prod_cost = 10

        self.MAX_COST = 999999999999999999999999
        self.NUM_CORES = multiprocessing.cpu_count()
        self.SEPARATOR = [-0.5, -0.5, 0]

    def get_instance(self, batch_size=3, consumers=15, include_separator=False):
        original_batch = []
        decorrelated_batch = []

        # Some samples have demand 0 and crash the batch generation. Discatd them
        while len(original_batch) < batch_size:
            b, d = self.sampler.get_grid_electricity(self.size, 0.4, consumers)

            # This is used for training in the combinatorial network
            #
            if include_separator:
                b = np.concatenate([b, self.SEPARATOR], axis=0)
                d = np.concatenate([d, self.SEPARATOR], axis=0)
            # print(len(b))
            if len(b) == consumers:
                original_batch.append(b)
                decorrelated_batch.append(d)

        original_batch = np.array(original_batch)
        decorrelated_batch = np.array(decorrelated_batch)
        # calc cost original batch
        cost_batch = self.calculate_cost_batch(original_batch)

        return original_batch, decorrelated_batch, cost_batch  # Generates a batch

    def get_repeated_instance(self, batch_size=3, consumers=15, include_separator=False):
        o, d, c = self.get_instance(1, consumers, include_separator)

        o = np.tile(o, [batch_size, 1, 1])
        d = np.tile(d, [batch_size, 1, 1])
        c = np.tile(c, [batch_size, 1])

        return o, d, c

    def calculate_cost_batch(self, original_batch):
        cost_batch = Parallel(n_jobs=self.NUM_CORES)(
            delayed(self.calc_cost_static)(original_batch[i, :, :]) for i in range(len(original_batch)))
        cost_batch = np.log(np.array(cost_batch))
        cost_batch = np.reshape(cost_batch, [-1, 1])
        return cost_batch

    def calc_cost(self, distributions_, input):
        """
        At the very to a batch wrapper
        There are going to be two wrappers. One to estimate the E[Reward|p_o:N]
        Does not take a batch. Takaes only one distribution
        :param distributions: the distributions for the instance only
        :param input: the instance
        :return:
        """
        # This fixed the input
        distributions = distributions_[:, 1]  # Only true
        p1 = []
        p2 = []
        samples = []
        for i in range(len(distributions)):
            s = np.random.choice([0, 1], p=[1 - distributions[i], distributions[i]])
            # Fixed output
            if s == 0:
                samples.append([1, 0])
            else:
                samples.append([0, 1])
            if s == 0:
                p1.append(input[i, :])
            else:
                p2.append(input[i, :])
        # Sampled class

        samples = np.array(samples)
        p1 = np.array(p1)
        p2 = np.array(p2)
        l1 = len(p1)
        l2 = len(p2)
        demand1 = 0
        demand2 = 0
        if len(p1) > 0:
            demand1 = np.sum(p1[:, 2])
        if len(p2) > 0:
            demand2 = np.sum(p2[:, 2])

        tsplen1 = 0
        tsplen2 = 0
        if l1 > 1:
            dm1 = pairwise_distances(p1[:, :2])
            s1 = Solver(tsp_size=l1)
            order1, tsplen1 = s1.run(dm1)
        if l2 >= 2:
            dm2 = pairwise_distances(p2[:, :2])
            s2 = Solver(tsp_size=l2)
            order2, tsplen2 = s2.run(dm2)
        cost = self.cost_function_(demand1, demand2, tsplen1, tsplen2)
        return samples, cost

    def cost_function_(self, demand1, demand2, tsplen1, tsplen2):

        # Note here demand needs to be >0 so that it has consumers! otherwise we do not have any producers
        con1 = 151441 * int(demand1 > 0) + 3600 * demand1 * tsplen1  # Nice since linear
        con2 = 151441 * int(demand2 > 0) + 3600 * demand2 * tsplen2

        p1 = 600000 * int(demand1 > 0) + 295000 * demand1
        p2 = 600000 * int(demand2 > 0) + 295000 * demand2
        cost = con1 + con2 + p1 + p2
        return cost

    def calc_cost_static(self, input_):
        points = input_[:, :2]
        # TODO use the weighted KMeans clustering
        clustering = AgglomerativeClustering(n_clusters=2).fit(points).labels_
        p1 = []
        p2 = []
        for i in range(len(clustering)):
            s = clustering[i]
            if s == 0:
                p1.append(input_[i, :])
            else:
                p2.append(input_[i, :])
        # Sampled class
        p1 = np.array(p1)
        p2 = np.array(p2)
        l1 = len(p1)
        l2 = len(p2)
        demand1 = 0
        demand2 = 0
        if len(p1) > 0:
            demand1 = np.sum(p1[:, 2])
        if len(p2) > 0:
            demand2 = np.sum(p2[:, 2])

        tsplen1 = 0
        tsplen2 = 0
        if l1 > 1:
            dm1 = pairwise_distances(p1[:, :2])
            s1 = Solver(tsp_size=l1)
            order1, tsplen1 = s1.run(dm1)
        if l2 >= 2:
            dm2 = pairwise_distances(p2[:, :2])
            s2 = Solver(tsp_size=l2)
            order2, tsplen2 = s2.run(dm2)
        cost = self.cost_function_(demand1, demand2, tsplen1, tsplen2)
        return cost

    def play_game_(self, distributions, inp):
        sampling, cost = self.calc_cost(distributions, inp)
        cost_static = self.calc_cost_static(inp)
        if cost < cost_static:
            return sampling, 1
        else:
            return sampling, -1

    def play_game_parallel_(self, stacked_input):
        """
        Calculates the game cost but is executable in parallel
        :param stacked_input:
        :return:
        """
        stacked_ = stacked_input[0, :, :]  # remove first dim, no need for it
        distributions = stacked_[:, :2]
        inp = stacked_[:, 2:]
        # Got the input from the tensor
        sampling, cost = self.calc_cost(distributions, inp)
        # cost_static = self.calc_cost_static(inp)

        # if cost < cost_static:
        #    return sampling, 1
        # else:
        #    return sampling, -1
        return sampling, cost

    def play_batch_game(self, batch_distr_, batch_inp_, times=1, cost_batch=None, normalized_batch_inp_=None):
        assert not (cost_batch is None)

        batch_distr = np.concatenate([batch_distr_ for _ in range(times)], axis=0)
        batch_problem = np.concatenate([batch_inp_ for _ in range(times)], axis=0)
        cost_batch = np.concatenate([cost_batch for _ in range(times)], axis=0)

        # If working with normalized data

        if not (normalized_batch_inp_ is None):
            batch_problem_normalized = np.concatenate([normalized_batch_inp_ for _ in range(times)], axis=0)
        input_parallel = np.concatenate([batch_distr, batch_problem], axis=2)
        input_list = np.vsplit(input_parallel, input_parallel.shape[0])
        print("Eval batch of len {}".format(len(batch_problem)))
        results = Parallel(n_jobs=self.NUM_CORES)(delayed(self.play_game_parallel_)(i) for i in tqdm(input_list))

        sample = []
        reward_ = []
        costs = []
        for counter, t in enumerate(results):
            sample.append(t[0])
            cstatic = cost_batch[counter, 0]
            # IF we want to change the reward:

            ones = np.ones(batch_distr.shape[1])

            our_costs = np.log(t[1])
            reward = np.sqrt(1/our_costs)

            # static_rewards.append(ones * static_reward)
            # reward = (t[1] <= cstatic) * 2 - 1
            reward_.append(ones * reward)
            costs.append(our_costs)
        # Normalized training data
        gc.collect()
        if not (normalized_batch_inp_ is None):
            return np.array(sample), np.array(reward_), batch_problem, batch_problem_normalized, np.array(costs)

        return np.array(sample), np.array(reward_), batch_problem, np.array(costs)
